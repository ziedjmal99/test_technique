Pour lancer le conteneur de l'application, vous devez d'abord effectuer le build  de l'image en utilisant la 
commande suivante :

docker build -t testtechnique .

Ensuite, pour démarrer le conteneur, utilisez la commande suivante :

docker run -d -p 80:80 

Cela permettra de lier le port 80 du conteneur au port 80 de votre machine hôte. Vous pouvez modifier le numéro de port selon vos besoins.




