import React, { useState, useEffect } from 'react';
import './datadisplay.css';

const Apidata = () => {
  const [data, setData] = useState(null);
  const [uaiFilter, setUaiFilter] = useState('');
  const url =
    'https://data.education.gouv.fr/api/records/1.0/search/?dataset=fr-en-dnma-par-uai-appareils&q=&rows=10&facet=debutsemaine&facet=uai';

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const jsonData = await response.json();
        setData(jsonData);
      } catch (error) {
        console.log('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  const handleUaiFilterChange = (event) => {
    setUaiFilter(event.target.value);
  };

  const filteredRecords = data ? data.records.filter((record) => record.fields.uai === uaiFilter) : [];

  return (
    <div>
      <h2>Fetching Data from API</h2>
      <div>
        <label htmlFor="uaiFilter">Filter by UAI:</label>
        <input type="text" id="uaiFilter" value={uaiFilter} onChange={handleUaiFilterChange} />
      </div>
      {data ? (
        <table className="data-table">
          <thead>
            <tr>
              <th>UAI</th>
              <th>Debutsemaine</th>
              <th>Visites Smartphone</th>
              <th>Visites Tablette</th>
              <th>Visites Ordinateur</th>
            </tr>
          </thead>
          <tbody>
            {filteredRecords.map((record) => (
              <tr key={record.recordid}>
                <td>{record.fields.uai}</td>
                <td>{record.fields.debutsemaine}</td>
                <td>{record.fields.visites_smartphone}</td>
                <td>{record.fields.visites_tablette}</td>
                <td>{record.fields.visites_ordinateur}</td>
              </tr>
            ))}
          </tbody>
        </table>
      ) : (
        <p>Loading data...</p>
      )}
    </div>
  );
};

export default Apidata;
