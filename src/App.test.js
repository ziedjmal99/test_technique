




import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import Apidata from './datadisplay';

describe('Apidata', () => {
  test('renders the component', () => {
    render(<Apidata />);

    expect(screen.getByLabelText('Filter by UAI:')).toBeInTheDocument();
  });

  test('updates the UAI filter', () => {
    render(<Apidata />);

    const input = screen.getByLabelText('Filter by UAI:');
    fireEvent.change(input, { target: { value: '0772223Y' } });

    expect(input.value).toBe('0772223Y');
  });
});









